import { Component } from '@angular/core';
import { UserData } from './data/UserData';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  data = UserData
}
