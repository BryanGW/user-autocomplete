import {AfterViewInit, Component, ElementRef, Input, ViewChild} from '@angular/core';

@Component({
  selector: 'app-user-name-autocomplete',
  templateUrl: './user-name-autocomplete.component.html',
  styleUrls: ['./user-name-autocomplete.component.scss']
})
export class UserNameAutocompleteComponent implements AfterViewInit {
  text = '';
  showUserAutocomplete = false;
  @Input('data') data = [];
  @ViewChild('input') inputElement: ElementRef = null;

  constructor() {
  }

  ngAfterViewInit(): void {
  }

  onKeyPress(event) {
    if (event.key === '@') {
      this.showUserAutocomplete = true;
    }
  }

  onAutocompleteSelect(username) {
    this.text = this.text + username;
    this.showUserAutocomplete = false;
    setTimeout(() => {
      this.inputElement.nativeElement.focus();
    });
  }

  onAutocompleteClose() {
    this.text = this.text.substr(0, this.text.length - 1);
    this.showUserAutocomplete = false;
    setTimeout(() => {
      this.inputElement.nativeElement.focus();
    });
  }

}
