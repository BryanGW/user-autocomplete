import {AfterViewInit, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';

@Component({
  selector: 'app-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.scss']
})
export class AutocompleteComponent implements AfterViewInit {
@Input('data') data = [];
  query = '';
  items = [];
  @ViewChild('input') inputElement: ElementRef = null;
  @Output('on-select') onSelectEmitter = new EventEmitter<any>();
  @Output('on-close') onCloseEmitter = new EventEmitter<any>();
  constructor() { }

  ngAfterViewInit(): void {
    this.inputElement.nativeElement.focus();
  }

  onInputModelChange(value){
    if(value === ''){
      this.onCloseEmitter.emit();
    }
    this.query = value;
    this.items = this.data.filter(item => {
      return item.username.indexOf(this.query) > -1
      || item.name.toLowerCase().indexOf(this.query) > -1;
    })
  }
  onKeyUp(event){
    if(event.key === 'Backspace' && !this.query){
      this.onCloseEmitter.emit();
    }
  }
  onClose(){
    this.onCloseEmitter.emit();
  }
  itemSelected(item){
    this.onSelectEmitter.emit(item.username);
  }

}
